# Docker-compose des web-apps utilisées au Louvain-li-Nux

## Liste des services:
- *traefik* : utilisé pour les reverse proxy des différentes services
- *cloud* : instance nextcloud
- *mattermost*: instance mattermost
- *portainer*: utilise pour la gestion des conteneurs, volumes et réseaux Docker
- *llnux_site*: site web du Louvain-li-Nuxµ
- ~~*borgmatic*: système de backups incrémentales~~
- *compta*: Gestion de comptes d'un KAP
- *llnux-inventory-site*: Gestion de la Patrimoine du Louvain-li-Nux

## requirements
Les packages suivant doivent être installés pour pouvoir déployer correctement les services:
- [Docker](https://docs.docker.com/get-docker/) 
- docker-compose

Pour build les images il sera nécessaire d'initialiser et d'update les submodules (une fois le dépôt cloné):

```shell
git submodule update --init --recursive
```

## Environment

Certains services ont besoin que certaines variables d'environnments soient définies pour docker-compose.

### Traefik (reverse proxy)

- `TRAEFIK_DASHBOARD_AUTH`: La valeur de l'option traefik [`traefik.http.middlewares.test-auth.basicauth.users`](https://doc.traefik.io/traefik/middlewares/http/basicauth/)

### Postgres (database)

- `POSTGRES_USER`: Le nom d'utilisateur de la database.
- `POSTGRES_PASSWORD`: Le mot de passe de la database.
- `POSTGRES_DB`: Le nom de la database.

### Mattermost

- `MM_USER`: Le nom d'utilisateur de la database.
- `MM_PASSWORD`: Le mot de passe de la database.
- `MM_DBNAME`: Le nom de la database.

### Wikijs

- `WIKIJS_USER`: Le nom d'utilisateur de la database.
- `WIKIJS_PASS`: Le mot de passe de la database.
- `WIKIJS_DB`: Le nom de la database.

### Inventory Site

- `INV_APP_KEYS` : La clé secrète pour le serveur.
- `INV_API_TOKEN_SALT` : La clé secrète pour la REST API.
- `INV_ADMIN_JWT_SECRET` : La clé secrète pour la panel administrateur.
- `INV_JWT_SECRET` : La clé secrète pour les plugins.
- `INV_DB_PASSWORD` : Le mot de passe de la database.

## How to?
Une fois le dépot cloné, les submodules à jour et les dépendances installées:

```
docker-compose build
docker-compose up -d
``` 

# Description détaillées des services:

## traefik

Permet de setup le reverse proxy des différentes services et assure l'https.

Pour forcer traefik à re-générer des certificats Let'sEncrypt, il suffit de remplacer `acme.json` par `acme.json.template` (pensez à le backup si nécessaire), d'appliquer les bonne permissions (traefik refuse quoi que ce soit de plus permissif que `600`) et de restart le docker traefik:

```shell
mv acme.json acme.json.backup
cp acme.json.template acme.json
chmod 600 acme.json
docker restart traefik
```

> [tracking issue](https://github.com/traefik/traefik/issues/3652)


## cloud
Le docker-compose a été créé en s'inspirant de la documentation nextcloud


## mattermost 
Le docker-compose a été créé en s'inspirant de la documentation mattermost


## portainer
Image de base [portainer](https://hub.docker.com/r/portainer/portainer)


## site (Louvain-li-Nux)
Un docker file a été créé et ajouté au [dépot du site](https://gitlab.com/louvainlinux/site) 

### Deployement
1. Update le submodule du site: `git submodule update --init --recursive site`
2. Reconstruire l'image du site et la lancer `docker-compose up -d --no-deps --build llnux_site` (--no-deps:ne démarre pas les services liés)


## borgmatic
Image de base [borgmatic](https://hub.docker.com/r/b3vis/borgmatic/), le fichier de config principal est `./borgmatic/borgmatic.d/config.yaml`
### Configuration:
[Configurer](https://torsion.org/borgmatic/docs/reference/configuration/) le fichier `./borgmatic/borgmatic.d/config.yaml`
#### Si nouveau serveur à backuper
- Créer une paire de clé SSH dans le dossier `./borgmatic/ssh`
- Ajouter la clé SSH publique créée au serveur de backup
- S'authentifier au moins une fois au serveur de backup pour que celui-ci soit connu du service, en effectuant la commande
  `ssh -i ./borgmatic/ssh/id_rsa -o UserKnownHostsFile=./borgmatic/ssh/known_file -p <port-ssh> <user-backup>@<server-backup>`
#### Si nouveau serveur de backup
- Installer le paquet borgbackup sur le serveur de backup
- Exécuter `docker exec sh -c 'borgmatic init -e authenticated'`


## compta

L'image est générée par [GitLab CI](https://gitlab.com/louvainlinux/compta/-/pipelines?scope=branches&page=1)
et push automatiquement sur le [GitLab Registry du dépôt](https://gitlab.com/louvainlinux/compta/container_registry).


## llnux-inventory-site

L'image est générée par [GitLab CI](https://gitlab.com/louvainlinux/inventory-site/-/pipelines?scope=branches&page=1)
et push automatiquement sur le [GitLab Registry du dépôt](https://gitlab.com/louvainlinux/inventory-site/container_registry).


# Crédits
Actuellement maintenu par l'équipe DevOps du Louvain-li-Nux
